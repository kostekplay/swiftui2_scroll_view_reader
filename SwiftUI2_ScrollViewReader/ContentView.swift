////  ContentView.swift
//  SwiftUI2_ScrollViewReader
//
//  Created on 07/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ScrollViewReader { scroll in
                ScrollView {
                    VStack (alignment: .leading, spacing: 10, content: {
                        Button(action: {
                            withAnimation {
                                scroll.scrollTo(100, anchor: .center)
                            }
                        }, label: {
                            /*@START_MENU_TOKEN@*/Text("Button")/*@END_MENU_TOKEN@*/
                        })
                        ForEach (0...1000, id: \.self) { (num) in
                            HStack {
                                Label(
                                    title: { Text("Position: \(num)")},
                                    icon: { Image(systemName: "house")}
                                )
                                Spacer()
                            }
                            .id(num)
                            .padding()
                        }
                    })
                }
                .navigationTitle("Scroll View")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
